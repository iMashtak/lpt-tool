def exec_cmd(cmd, stdout_pipe=True):
    import subprocess
    if stdout_pipe:
        cmd_process = subprocess.run(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            encoding='utf-8',
            universal_newlines=True
        )
        return cmd_process
    else:
        cmd_process = subprocess.run(
            cmd,
            stderr=subprocess.STDOUT,
            encoding='utf-8',
            universal_newlines=True
        )
        return cmd_process


def write_file(path, content):
    with open(path, 'w') as f:
        f.write(content)


def cwd_with(path):
    import os
    return os.path.join(os.getcwd(), path)


def to_yaml(obj, options=None):
    # From https://stackoverflow.com/a/63179923
    import ruamel.yaml
    yaml = ruamel.yaml.YAML()
    yaml.indent(mapping=2, sequence=4, offset=2)
    if options is None:
        options = {}
    from io import StringIO
    string_stream = StringIO()
    yaml.dump(obj, string_stream, **options)
    output_str = string_stream.getvalue()
    string_stream.close()
    return output_str


def read_yaml_from_file(path):
    import ruamel.yaml
    yaml = ruamel.yaml.YAML()
    with open(path, 'r') as f:
        return yaml.load(f)


def git_clone(repo, path):
    git_process = exec_cmd(["git", "clone", repo, path])
    if git_process.returncode == 0 or git_process.returncode == 128:
        return
    else:
        raise Exception(git_process.stdout)


def git_fetch(path):
    import os
    cwd = os.getcwd()
    os.chdir(path)
    git_process = exec_cmd(["git", "fetch"])
    os.chdir(cwd)
    if git_process.returncode != 0:
        raise Exception(git_process.stdout)


def git_checkout(path, tag):
    import os
    cwd = os.getcwd()
    os.chdir(path)
    git_process = exec_cmd(["git", "checkout", tag])
    os.chdir(cwd)
    if git_process.returncode != 0:
        raise Exception(git_process.stdout)


def git_tag(path):
    import os
    cwd = os.getcwd()
    os.chdir(path)
    git_process = exec_cmd(["git", "tag"])
    os.chdir(cwd)
    if git_process.returncode != 0:
        raise Exception(git_process.stdout)
    else:
        return list(map(lambda x: x.strip(), git_process.stdout.split(os.linesep)[:-1]))


def rm_rf(path):
    rm_process = exec_cmd(["rm", "-rf", path])
    if rm_process.returncode != 0:
        raise Exception(rm_process.stdout)
