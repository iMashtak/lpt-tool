import core.utils as u
import os

LPT_PATH = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + "/../")


class LptTool:
    LPT_NAME_FILE = ".lpt-name"

    def __init__(self):
        with open(os.path.join(LPT_PATH, LptTool.LPT_NAME_FILE), "r") as f:
            self.name = f.read().strip("\r\n ")


class Version:
    VERSION_FILE = ".version"

    def __init__(self):
        with open(os.path.join(LPT_PATH, Version.VERSION_FILE), "r") as v:
            self.raw_version = v.read()

    def raw(self):
        return self.raw_version

    @staticmethod
    def match(mask, versions, current=None):
        vn = []
        for version in versions:
            parts = version[1:].split(".")
            vn.append(int(parts[0]) * (100 ** 2) + int(parts[1]) * (100 ** 1) + int(parts[2]) * (100 ** 0))

        def max_version(vnl):
            max_v = max(vnl)
            return f"v{max_v // (100 ** 2) % 100}.{max_v // (100 ** 1) % 100}.{max_v // (100 ** 0) % 100}"

        if mask == "$":
            return current
        if mask == "*":
            return max_version(vn)
        mask_parts = mask.split(".")
        if len(mask_parts) == 2:
            major = mask_parts[0]
            return max_version(list(filter(lambda x: x // (100 ** 2) % 100 == int(major), vn)))
        if len(mask_parts) == 3:
            major = mask_parts[0]
            minor = mask_parts[1]
            return max_version(
                list(filter(lambda x: x // (100 ** 2) % 100 == int(major) and x // (100 ** 1) % 100 == int(minor), vn)))
        return None


class Workspace:
    WORKSPACE_FILE = "lpt-workspace.yaml"
    SRC_DIR = "src"
    STORE_DIR = ".store"
    GITIGNORE_FILE = ".gitignore"
    VSCODE_DIR = ".vscode"

    GITIGNORE_CONTENT = (
        ".store\n"
        "*.pdf\n"
        "out\n"
    )

    def __init__(self, name=None, repos=None):
        if name is not None:
            self.name = name
            self.repos = repos
            self.version = Version()
            return
        workspace_path = Workspace.find_workspace(os.getcwd())
        if workspace_path is None:
            raise Exception(f"Not found workspace on path '{os.getcwd()}'")
        workspace_yaml = u.read_yaml_from_file(os.path.join(workspace_path, Workspace.WORKSPACE_FILE))
        self.name = workspace_yaml["name"]
        self.repos = workspace_yaml["repos"]
        self.version = workspace_yaml["version"]
        self.path = workspace_path

    def to_dto(self):
        return {
            "name": self.name,
            "repos": self.repos,
            "version": self.version.raw()
        }

    @staticmethod
    def init(name):
        if Workspace.exists_on_path(os.getcwd()):
            print(f"ERROR: Workspace have already exist on path '{os.getcwd()}'")
            return
        workspace = Workspace(name, ["https://gitlab.com/iMashtak/lpt-default-resources.git"])
        u.write_file(u.cwd_with(Workspace.WORKSPACE_FILE), u.to_yaml(workspace.to_dto()))
        os.mkdir(u.cwd_with(Workspace.SRC_DIR))
        u.write_file(u.cwd_with(Workspace.GITIGNORE_FILE), Workspace.GITIGNORE_CONTENT)
        os.mkdir(Workspace.STORE_DIR)
        u.write_file(u.cwd_with(LptTool.LPT_NAME_FILE), LptTool().name)

    @staticmethod
    def exists_on_path(path):
        return Workspace.find_workspace(path) is not None

    @staticmethod
    def find_workspace(path):
        if os.path.isfile(os.path.join(path, Workspace.WORKSPACE_FILE)):
            return path
        else:
            if path == "/":
                return None
            return Workspace.find_workspace(os.path.dirname(path))

    def update(self):
        for repo in self.repos:
            path = self.get_repo_path(repo)
            u.git_clone(repo, path)
            u.git_fetch(path)

    @staticmethod
    def repo_dir_name(repo):
        return os.path.join(*repo[:-4].split("/")[2:])

    def get_repo_path(self, repo):
        return os.path.join(self.path, Workspace.STORE_DIR, Workspace.repo_dir_name(repo))

    def list_repos(self):
        repos = []
        for repo in self.repos:
            repo_path = self.get_repo_path(repo)
            repos.append(Repo(repo_path, repo))
        return repos

    def get_resource_path(self, resource_type, resource_name):
        for repo in self.list_repos():
            path = repo.get_resource_path(resource_type, resource_name)
            if path is not None:
                return path

    def add_vscode_support(self):
        vscode_path = os.path.join(self.path, Workspace.VSCODE_DIR)
        if not os.path.exists(vscode_path):
            os.makedirs(os.path.join(self.path, Workspace.VSCODE_DIR))
        if os.path.isfile(os.path.join(vscode_path, "settings.json")):
            print(
                "Ошибка: настройки для Visual Studio Code уже заданы для данного репозитория. "
                "Требуемые опции будут записаны в файл '.vscode/lpt.settings.json', однако не будут применены. "
                "Встройте содержимое этого файла в ваш файл '.vscode/settings.json' "
                "для обеспечения корректной работы."
            )
            with open(os.path.join(LPT_PATH, "static", "vscode.settings.json"), "r") as f:
                u.write_file(os.path.join(vscode_path, "lpt.settings.json"), f.read())
        else:
            with open(os.path.join(LPT_PATH, "static", "vscode.settings.json"), "r") as f:
                u.write_file(os.path.join(vscode_path, "settings.json"), f.read())

    def get_repo_by_resource_namespace(self, namespace):
        repos = list(filter(lambda x: x.namespace == namespace, self.list_repos()))
        if len(repos) == 1:
            return repos[0]
        else:
            raise Exception(f"Несколько репозиториев объявляют пространство имён '{namespace}'")


class Document:
    DOCUMENT_FILE = "lpt-document.yaml"
    RESOURCES_DIR = "resources"
    IMG_DIR = "img"
    OUT_DIR = "out"
    LIBRARY_DIR = "library"

    def __init__(self, name=None, payload=None, workspace=None):
        if payload is not None:
            self.name = payload["name"]
            self.template = payload["template"]
            self.resources = {}
            self.build_packs = {}
            self.profiles = {}
            return
        workspace = workspace if workspace is not None else Workspace()
        document_file = u.read_yaml_from_file(
            os.path.join(Document.get_document_path(workspace.path, name), Document.DOCUMENT_FILE)
        )
        self.workspace = workspace
        self.name = name
        self.path = Document.get_document_path(workspace.path, name)
        self.template = document_file["template"]
        self.resources = document_file["resources"]
        self.build_packs = document_file["build-packs"]
        self.profiles = document_file["profiles"]

    def to_dto(self):
        return {
            "name": self.name,
            "template": self.template,
            "resources": self.resources,
            "build-packs": self.build_packs,
            "profiles": self.profiles,
        }

    @staticmethod
    def init(name, template=None):
        workspace = Workspace()
        document_dir_path = Document.get_document_path(workspace.path, name)
        document_file_path = os.path.join(document_dir_path, Document.DOCUMENT_FILE)
        if template is not None:
            repos = workspace.list_repos()
            template_path_alts = []
            for repo in repos:
                t = repo.get_resource_path(Repo.TEMPLATES, template)
                if t is not None:
                    template_path_alts.append(t)
            if len(template_path_alts) == 0:
                raise Exception(f"Template '{template}' not found in defined repos")
            if len(template_path_alts) > 1:
                raise Exception(f"There are several templates with name '{template}' in defined repos. "
                                f"Resolve conflict and retry")
            template_path = template_path_alts[0]
            from distutils.dir_util import copy_tree
            copy_tree(template_path, document_dir_path)
            document = Document(name, workspace=workspace)
            document.template = template
            document.name = name
            u.write_file(document_file_path, u.to_yaml(document.to_dto()))
        else:
            document = Document(payload={"name": name, "template": "empty"})
            os.makedirs(document_dir_path, exist_ok=True)
            u.write_file(document_file_path, u.to_yaml(document.to_dto()))
        os.makedirs(os.path.join(document_dir_path, Document.RESOURCES_DIR), exist_ok=True)
        os.makedirs(os.path.join(document_dir_path, Document.IMG_DIR), exist_ok=True)
        os.makedirs(os.path.join(document_dir_path, Document.OUT_DIR), exist_ok=True)
        os.makedirs(os.path.join(document_dir_path, Document.LIBRARY_DIR), exist_ok=True)

    @staticmethod
    def get_document_path(workspace_path, name):
        return os.path.join(workspace_path, Workspace.SRC_DIR, name)

    def build(self, profile_name=None, build_pack_name=None):
        profile, profile_name, build_pack, build_pack_name = self.__define_build_inputs(profile_name, build_pack_name)
        cmd = []
        if build_pack["type"] == "docker":
            cmd.extend([
                "docker", "run", "--rm", "-i",
                "-v", f"{self.path}:{build_pack['options']['volume']}",
                build_pack["options"]["image"]
            ])
        elif build_pack["type"] == "native":
            pass
        else:
            raise Exception(f"Unknown build-pack type '{build_pack['type']}'")
        cmd.extend([
            "latexmk", "-synctex=1", "-interaction=nonstopmode", "-file-line-error", "-xelatex",
            f"-outdir=out",
            profile["root"]
        ])
        build_process = u.exec_cmd(cmd, stdout_pipe=False)
        if build_process.returncode != 0:
            raise Exception(f"Document build failed: profile={profile_name}, build_pack={build_pack_name}")

    def __define_build_inputs(self, profile_name, build_pack_name):
        profile = None
        build_pack = None
        if profile_name is None:
            for key, value in self.profiles.items():
                if "default" in value and value["default"] is True:
                    profile_name = key
                    profile = value
        else:
            profile = self.profiles[profile_name]
        if build_pack_name is None:
            for key, value in self.build_packs.items():
                if "default" in value and value["default"] is True:
                    build_pack_name = key
                    build_pack = value
        else:
            build_pack = self.build_packs[build_pack_name]
        return profile, profile_name, build_pack, build_pack_name

    @staticmethod
    def build_document(document_path):
        parts = document_path.split(os.sep)
        document_name = parts[-2]
        profile_root = parts[-1] + ".tex"
        document = Document(document_name)
        ok = False
        for profile_name, profile in document.profiles.items():
            if profile["root"] == profile_root:
                document.build(profile_name)
                ok = True
                break
        if not ok:
            raise Exception(f"Not found document on path: '{document_path}'")

    def update(self):
        from distutils.dir_util import copy_tree

        def get_resource_path(document, rsrc_name, rsrc_type, rsrc_version, current_version=None):
            repo = document.workspace.get_repo_by_resource_namespace(rsrc_name.split(Repo.OBJECT_DELIMITER)[0])
            versions = repo.get_versions()
            matched_version = Version.match(rsrc_version, versions, current_version)
            u.git_checkout(repo.path, matched_version)
            path = self.workspace.get_resource_path(rsrc_type, rsrc_name)
            if path is None:
                raise Exception(f"Not found resource '{rsrc_name}' in {rsrc_type}")
            return path, matched_version, repo

        def cp_resource(from_path, rsrc_type, rsrc_name):
            copy_tree(from_path, os.path.join(
                self.path, Document.RESOURCES_DIR,
                rsrc_type, rsrc_name.replace(Repo.OBJECT_DELIMITER, os.sep)
            ))

        def update_resource(document, rsrc, current_version, rsrc_repo):
            for inner_rsrc_name, inner_rsrc_version in rsrc.dependencies.items():
                if Repo.OBJECT_DELIMITER not in inner_rsrc_name:
                    inner_rsrc_name = f"{rsrc_repo.namespace}{Repo.OBJECT_DELIMITER}{inner_rsrc_name}"
                rsrc_path, matched_rsrc_version, inner_repo = get_resource_path(
                    document, inner_rsrc_name,
                    rsrc.type, inner_rsrc_version, current_version
                )
                cp_resource(rsrc_path, rsrc.type, inner_rsrc_name)
                inner_rsrc = Resource(rsrc_repo, rsrc_path)
                update_resource(document, inner_rsrc, matched_rsrc_version, inner_repo)

        for resource_type, resource_defs in self.resources.items():
            for resource_name, resource_version in resource_defs.items():
                resource_path, matched_resource_version, resource_repo = get_resource_path(
                    self, resource_name, resource_type, resource_version
                )
                cp_resource(resource_path, resource_type, resource_name)
                resource = Resource(resource_repo, resource_path)
                update_resource(self, resource, matched_resource_version, resource_repo)

    def delete(self, yes=False):
        if not yes:
            y = input(f"Подтвердите удаление всех файлов по пути '{self.path}' [y/n]: ")
            if y == "y" or y == "yes":
                yes = True
            else:
                print("Удаление отменено")
                return
        u.rm_rf(self.path)


class Repo:
    REPO_MANIFEST_FILE = "lpt-repo-manifest.yaml"
    NAMESPACE_DELIMITER = "."
    OBJECT_DELIMITER = "::"

    STYLES = "styles"
    TEMPLATES = "templates"
    PREAMBLES = "preambles"

    def __init__(self, path, url):
        repo = u.read_yaml_from_file(os.path.join(path, Repo.REPO_MANIFEST_FILE))
        self.namespace = repo["namespace"]
        self.resources = repo["resources"]
        self.path = path
        self.url = url

    def get_resource_path(self, resource_type, resource_name):
        for key, value in self.resources[resource_type].items():
            if resource_name == f"{self.namespace}{Repo.OBJECT_DELIMITER}{key}":
                return os.path.join(self.path, value["path"])

    def get_versions(self):
        return u.git_tag(self.path)


class Resource:
    STYLE_TYPE = Repo.STYLES
    TEMPLATE_TYPE = Repo.TEMPLATES
    PREAMBLE_TYPE = Repo.PREAMBLES
    RESOURCE_MANIFEST_FILE = "lpt-resource-manifest.yaml"

    def __init__(self, repo, path):
        resource = u.read_yaml_from_file(os.path.join(path, Resource.RESOURCE_MANIFEST_FILE))
        self.type = resource["type"]
        self.dependencies = resource["dependencies"]
        self.repo = repo
