from abc import ABC
import os

LPT_PATH = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + "/../")


def run_lpt_cmd(args):
    objects = {
        "version": VersionCmd(),
        "workspace": WorkspaceCmd(),
        "document": DocumentCmd(),
        "tool": ToolCmd()
    }

    args_dict = args.__dict__
    spec_keys = list(filter(lambda x: x.startswith("_") and args_dict[x], args_dict))
    if len(spec_keys) > 1:
        raise Exception("unexpected count of special keys")
    if len(spec_keys) == 1:
        parts = spec_keys[0].split("_")[1:]
        objects[parts[0]].do(parts[1], None)
    else:
        objects[args.object].do(args.action, args)


class Cmd(ABC):
    def do(self, action, args):
        pass


class VersionCmd(Cmd):
    SHOW_CMD = "show"

    def do(self, action, args):
        from core.objects import Version
        if action == VersionCmd.SHOW_CMD:
            print(f"v{Version().raw()}")
        else:
            raise Exception(f"Unknown action '{action}' for version")


class WorkspaceCmd(Cmd):
    INIT_ACTION = "init"
    UPDATE_ACTION = "update"
    LS_ACTION = "ls"
    CREATE_ACTION = "create"
    VSCODE_ACTION = "add-vscode-support"

    def do(self, action, args):
        from core.objects import Workspace
        from core.objects import Repo
        if action == WorkspaceCmd.INIT_ACTION:
            Workspace.init(args.name)
        elif action == WorkspaceCmd.UPDATE_ACTION:
            workspace = Workspace()
            workspace.update()
        elif action == WorkspaceCmd.CREATE_ACTION:
            self.do(WorkspaceCmd.INIT_ACTION, args)
            self.do(WorkspaceCmd.UPDATE_ACTION, args)
        elif action == WorkspaceCmd.LS_ACTION:
            workspace = Workspace()
            repos = workspace.list_repos()
            for repo in repos:
                print(f"Repo {repo.url}:")
                print("  Styles:")
                for style in repo.resources[Repo.STYLES]:
                    print(f"    {repo.namespace}{Repo.OBJECT_DELIMITER}{style}")
                print("  Preambles:")
                for preamble in repo.resources[Repo.PREAMBLES]:
                    print(f"    {repo.namespace}{Repo.OBJECT_DELIMITER}{preamble}")
                print("  Templates:")
                for template in repo.resources[Repo.TEMPLATES]:
                    print(f"    {repo.namespace}{Repo.OBJECT_DELIMITER}{template}")
        elif action == WorkspaceCmd.VSCODE_ACTION:
            workspace = Workspace()
            workspace.add_vscode_support()
        else:
            raise Exception(f"Unknown action '{action}' for workspace")


class DocumentCmd(Cmd):
    INIT_ACTION = "init"
    BUILD_ACTION = "build"
    UPDATE_ACTION = "update"
    CREATE_ACTION = "create"
    DELETE_ACTION = "delete"

    def do(self, action, args):
        from core.objects import Document
        if action == DocumentCmd.INIT_ACTION:
            Document.init(args.name, args.template)
        elif action == DocumentCmd.BUILD_ACTION:
            if args.name.startswith("[file]:"):
                Document.build_document(args.name[len("[file]:"):])
            else:
                document = Document(args.name)
                document.build(args.profile_name, args.build_pack_name)
        elif action == DocumentCmd.UPDATE_ACTION:
            document = Document(args.name)
            document.update()
        elif action == DocumentCmd.CREATE_ACTION:
            self.do(DocumentCmd.INIT_ACTION, args)
            self.do(DocumentCmd.UPDATE_ACTION, args)
        elif action == DocumentCmd.DELETE_ACTION:
            document = Document(args.name)
            document.delete()
        else:
            raise Exception(f"Unknown action '{action}' for document")


class ToolCmd(Cmd):
    UPGRADE_ACTION = "upgrade"

    def do(self, action, args):
        import core.utils as u
        from core.objects import Version
        if action == ToolCmd.UPGRADE_ACTION:
            old = Version().raw()
            u.git_pull(LPT_PATH)
            new = Version().raw()
            print(f"Upgraded from v{old} to v{new}")
        else:
            raise Exception(f"Unknown action '{action}' for tool")
