import argparse
import os
import sys
import traceback

LPT_PATH = os.path.dirname(os.path.abspath(__file__))
sys.path.append(LPT_PATH)
sys.path.append(LPT_PATH + "/lib")

parser = argparse.ArgumentParser(
    description=f"Latex Papers Tool"
)
parser.add_argument("-v", "--version", dest="_version_show", action="store_true")

subparsers = parser.add_subparsers(
    title="objects",
    description="",
    required=False,
    dest="object"
)


def make_object(object_name, description=""):
    object_parser = subparsers.add_parser(object_name)
    object_subparsers = object_parser.add_subparsers(
        title="actions",
        description=description,
        dest="action",
        required=False
    )

    def with_action(action_name, *action_arguments):
        p = object_subparsers.add_parser(action_name)
        for a in action_arguments:
            if "action" not in a:
                a["action"] = "store"
            p.add_argument(*a["name"], action=a["action"])
        return object_subparsers

    object_subparsers.__dict__["with_action"] = with_action
    return object_subparsers


make_object("version") \
    .with_action("show")

make_object("workspace") \
    .with_action("init",
                 {"name": ["name"]}
                 ) \
    .with_action("update") \
    .with_action("ls") \
    .with_action("create",
                 {"name": ["name"]}
                 ) \
    .with_action("add-vscode-support")

make_object("document") \
    .with_action("init",
                 {"name": ["name"]},
                 {"name": ["-t", "--template"]}
                 ) \
    .with_action("build",
                 {"name": ["name"]},
                 {"name": ["-p", "--profile-name"]},
                 {"name": ["-b", "--build-pack-name"]}
                 ) \
    .with_action("update",
                 {"name": ["name"]}
                 ) \
    .with_action("create",
                 {"name": ["name"]},
                 {"name": ["-t", "--template"]}
                 ) \
    .with_action("delete",
                 {"name": ["name"]},
                 {"name": ["-y", "--yes"], "action": "store_true"}
                 )

make_object("tool") \
    .with_action("upgrade")

parsed_args = parser.parse_args()
try:
    from core.cmds import run_lpt_cmd

    run_lpt_cmd(parsed_args)
except Exception:
    print(traceback.format_exc())
    exit(1)
