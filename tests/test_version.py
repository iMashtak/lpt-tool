import pytest


def test_match():
    from core.objects import Version
    version = Version()
    assert version.match("$", ["v1.5.1", "v1.0.2", "v2.0.0"], "v1.3.8") == "v1.5.1"
