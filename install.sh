#!/bin/bash
LPT_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
LPT_LINK=lpt
while :
do
    echo "> Creating symlink '${LPT_LINK}'"
    which "${LPT_LINK}" >> /dev/null
    if [ $? -eq 0 ]; then
        echo "> Symlink '${LPT_LINK}' have already exists"
        read -p "< Please, enter new name for lpt-tool: " LPT_LINK
    else
        ln -s "${LPT_PATH}/run.sh" "/usr/local/bin/${LPT_LINK}"
        break
    fi
done

if [ $? -eq 0 ]; then
    echo "> Installation failed"
    exit 1
else
    echo ${LPT_LINK} > "${LPT_PATH}/.lpt-name"
    echo "> Installation successful"
fi
